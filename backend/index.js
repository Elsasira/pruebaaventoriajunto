const express = require('express')
const cors = require('cors')
const fileUpload = require('express-fileupload')
const path = require('path')

//VALIDATIONS
const validations = require('./repositories/user_Repositories/user_Validations.js')
const validReservation = require('./repositories/reservation_Repositories/reservation_Validation.js')
const validReservationExperience = require('./repositories/reservation_experience_Repositories/reservation_experience_Validations.js')
const validExperiences = require('./repositories/experience_Repositories/experience_Validations.js')

const { login } = require('./helpers/authentication.js')
const user = require('./repositories/user_Repositories/user_Repository.js')
const reservations = require('./repositories/reservation_Repositories/reservation_Repository.js')
const reservationExperience = require('./repositories/reservation_experience_Repositories/reservation_experience_Repository.js')
const experiences = require('./repositories/experience_Repositories/experience_Repository.js')
const city = require('./repositories/city_Repositories/city_Repository.js')
const { getExperiencesPhotos } = require('./repositories/photo_Repositories/photo_Repository.js')
const { searchExperience } = require('./repositories/search_Repositories/search_Repository.js')
const { parseData } = require('./helpers/parseData.js')


const app = express()
app.use(express.json())
app.use(cors())
app.use(fileUpload({}))
app.use('/static', express.static(__dirname + '/uploadImages'))
app.use('/static2', express.static(__dirname + '/userPhotos'))


app.post('/api/register', validations.isValidDataUser, user.register, validations.isValidUserFormData, login)
app.post('/api/login', validations.isValidLogin, validations.isValidUser, validations.isValidPassword, login)
app.post('/api/editUser', validations.isLogged, validations.isValidUserFormData, user.editUser)
app.post('/api/viewDataUser', validations.isLogged, user.get)
app.post('/api/comparePassword', validations.isLogged, validations.isValidUser, validations.isValidComparePassword)
app.post('/api/changePassword', validations.isLogged, validations.isValidUser, user.changePassword)

app.post('/api/reservation', validations.isLogged, validReservation.isValidReservation, reservations.reservation)
app.patch('/api/cancelReservation', validations.isLogged, reservations.cancelReservation)
app.post('/api/rating', validations.isLogged, validReservationExperience.isValidRating, reservationExperience.rating)
app.get('/api/viewReservation', validations.isLogged, reservations.viewReservation)

app.post('/api/createExperience', validations.isLogged, validations.isAdministrator, validExperiences.isValidImage, parseData, validExperiences.isValidExperience, experiences.newExperience)
app.post('/api/editExperience', validations.isLogged, validations.isAdministrator, validExperiences.isValidUpdateExperience, experiences.editExperience)

app.get('/api/viewBestCities', city.viewCityMoreExperiences)
app.get('/api/viewBestExperiences', experiences.viewBestExperience)
app.get('/api/viewAllExperiences', experiences.viewAllExperience)
app.get('/api/viewOneExperience/:id_experiencia', experiences.getExperiences)
app.get('/api/viewFiveExperiences', experiences.viewFiveExperience)
app.get('/api/viewCities', city.showCities)


app.get('/api/viewExperiencesPhotos/:id_experiencia', getExperiencesPhotos)
app.delete('/api/viewExperiencesPhotos/:id_experiencia', getExperiencesPhotos)

app.get('/api/searchExperience', searchExperience)




app.listen(3500, () => console.log('Escuchando!'))