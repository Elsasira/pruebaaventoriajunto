const { getConnection } = require('../connectors.js')

const insertExperienceCities = 'INSERT INTO experiencias_ciudades (id_experiencia, id_ciudad) VALUES (?, ?)'

async function experiencesCities(req, res, idExperience, idCity)
{
    try
    {
        const connection = await getConnection()
        await connection.query(insertExperienceCities,[idExperience, idCity] )
    }
    catch (error)
    {
        console.error(error.message)
        res.statusCode = 401
        res.send({error: 'No es posible conectar, inténtelo más tarde'})
    }
}

module.exports = { experiencesCities }