const { getConnection } = require('../connectors.js')

const insertExperiencePhotos = 'INSERT INTO fotos (id_experiencia, ruta) VALUES (?, ?)'
const getExperiencePhotos = 'SELECT ruta FROM fotos WHERE id_experiencia = ?'
const deleteExperiencePhotos = 'DELETE FROM fotos WHERE id_experiencia = ?'


async function experiencesPhotos(req, res, idExperience, route)
{
    try
    {
        const connection = await getConnection()
        await connection.query(insertExperiencePhotos,[idExperience, route]) 
    }
    catch (error)
    {
        console.error(error.message)
        res.statusCode = 401
        res.send({error: 'No es posible conectar, inténtelo más tarde'})
    }
}

async function getExperiencesPhotos(req, res)
{
    try
    {
        const { id_experiencia} = req.params
        const connection = await getConnection()
        const [photos] = await connection.query(getExperiencePhotos,[id_experiencia]) 

        res.statusCode = 200
        res.send(photos)    
    }
    catch (error)
    {
        console.error(error.message)
        res.statusCode = 401
        res.send({error: 'No es posible conectar, inténtelo más tarde'})
    }
}

async function deleteExperiencesPhotos(req, res)
{
    try
    {
        const { id_experiencia} = req.params
        const connection = await getConnection()
        await connection.query(deleteExperiencePhotos,[id_experiencia]) 

        res.statusCode = 200
        res.send({ok: 'Imagen borrada correctamente'})
    }
    catch (error)
    {
        console.error(error.message)
        res.statusCode = 401
        res.send({error: 'No es posible conectar, inténtelo más tarde'})
    }
}

module.exports = { experiencesPhotos, getExperiencesPhotos, deleteExperiencesPhotos  }