const joi = require('joi')

const schemaReservation = joi.object(
    {
        id: joi.number().required(),
        fecha_actividad: joi.date().required(),
        precio: joi.number().required(),
        cantidad_reservada: joi.number().required()

    })

async function isValidReservation(req, res, next)
{
    try
    {
        await schemaReservation.validateAsync(req.body[0])
        next()
    } 
    catch (error)
    {
        console.error(error.message)
        res.statusCode = 404
        res.send({error: 'Alguno de los datos introducidos no es válido'})
    }
}

module.exports = { isValidReservation }