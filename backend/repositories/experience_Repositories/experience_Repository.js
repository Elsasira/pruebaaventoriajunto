
const uuid = require('uuid')
const path = require('path')
const { getConnection } = require('../connectors.js')
const { experiencesCities } = require('../../repositories/experience_city_Repositories/experience_city_Repository.js')
const { experiencesPhotos } = require('../../repositories/photo_Repositories/photo_Repository.js')

const getDataExperience = 'SELECT e.id, e.nombre, e.titulo, e.descripcion, e.duracion, e.direccion, e.precio, e.`plazas_totales`, e.`dias_actividad`, e.estado as `estado_experiencia`, SUM(ifnull(r.`cantidad_reservada`, 0)) AS`plazas_ocupadas`, (e.`plazas_totales` - SUM(ifnull(r.`cantidad_reservada`, 0))) AS`plazas_disponibles` FROM experiencias e LEFT JOIN`reservas_experiencias` re ON e.id = re.`id_experiencia` LEFT JOIN reservas r ON re.`id_reserva` = r.id WHERE e.id =? AND e.estado LIKE 1 GROUP BY e.id'
const insertExperience = 'INSERT INTO `experiencias` (nombre, titulo, descripcion,duracion, direccion, precio, plazas_totales, dias_actividad, estado) VALUES (?,?,?,?,?,?,?,?,true)'
const updateExperience = 'UPDATE experiencias SET nombre = ?, titulo = ?, descripcion = ?,duracion = ?, direccion=?, precio = ?, plazas_totales =?, dias_actividad = ?, estado = ? WHERE id = ?'
const getBestExperience = 'SELECT re.id_experiencia, e.titulo, sum(valoracion) as puntos_valoraciones, MAX(f.ruta) as ruta FROM aventoria.reservas_experiencias re INNER JOIN aventoria.experiencias e ON re.id_experiencia=e.id  LEFT JOIN `fotos` f ON e.id=f.id_experiencia GROUP BY re.id_experiencia ORDER BY puntos_valoraciones DESC LIMIT 6'
const getAllExperience = 'SELECT e.*, MAX(f.ruta) as ruta, c.nombre as ciudad FROM`experiencias` e LEFT JOIN`fotos` f ON e.id = f.id_experiencia LEFT JOIN experiencias_ciudades ec ON e.id = ec.id_experiencia LEFT JOIN ciudades c ON ec.id_ciudad = c.id GROUP BY e.id  ORDER BY`nombre` ASC'
const getExperiencePhotos = 'SELECT ruta FROM fotos WHERE id_experiencia = ?'
const getExperienceComment = 'SELECT re.comentario, re.valoracion FROM experiencias e LEFT JOIN `reservas_experiencias` re ON e.id=re.`id_experiencia` WHERE e.id=? AND e.estado=1 AND re.estado LIKE "aprobado"'
const getExperienceCity = 'SELECT c.nombre as Ciudad FROM experiencias e INNER JOIN `experiencias_ciudades` ec ON e.id=ec.`id_exaperiencias` INNER JOIN ciudades c ON ec.`id_ciudad` = c.id WHERE e.id = ?'
const getFiveExperience = 'SELECT e.id, e.titulo, MAX(f.ruta) as ruta FROM`experiencias` e LEFT JOIN`fotos` f ON e.id = f.id_experiencia  GROUP BY e.id ORDER BY RAND() LIMIT 5'
const getCityId = 'SELECT id FROM ciudades WHERE nombre = ?'
const insertCity = 'INSERT INTO ciudades (nombre) VALUES (?)'

async function getExperiences(req, res) {
    try {
        const { id_experiencia } = req.params
        const connection = await getConnection()
        const [experience] = await connection.query(getDataExperience, id_experiencia)
        const [photos] = await connection.query(getExperiencePhotos, id_experiencia)
        const [comments] = await connection.query(getExperienceComment, id_experiencia)
        const routes = photos.map(photo => photo.ruta)


        res.statusCode = 200
        res.send({ ...experience[0], photos: routes, comments })
    }
    catch (error) {
        console.error(error.message)
        res.statusCode = 401
        res.send({ error: 'La experiencia no existe' })
    }
}

async function newExperience(req, res) {
    try {
        const connection = await getConnection()
        const dataExperience = req.body

        const objExperiences = Object.values({ ...dataExperience })
        console.log('datos', req.body.ciudad)
        const getIdExperience = await connection.query(insertExperience, objExperiences)

        const [row] = await connection.query(getCityId, req.body.ciudad)

        let getIdCity = row[0].id

        if(!getIdCity){
            getIdCity = await connection.query(insertCity, req.body.ciudad)
        }

        await experiencesCities(req, res, getIdExperience[0].insertId, getIdCity)

        const files = req.files.image

        files.map(async (file) => {
            const route = getIdExperience[0].insertId + '_' + uuid.v4() + '.jpg'
            file.mv(path.join(__dirname + '/../../uploadImages/' + route))

            await experiencesPhotos(req, res, getIdExperience[0].insertId, route)
        })
        const idExpe=getIdExperience[0].insertId.toString()
        res.statusCode = 200
        console.log(idExpe);
        res.send(idExpe)
    }
    catch (error) {
        console.error(error.message)
        res.statusCode = 401
        res.send({ error: 'No se pudo introducir la experiencia' })
    }
}

async function editExperience(req, res) {
    try {
        const connection = await getConnection()
        const dataExperience = Object.values({ ...req.body, id: req.body.id_experiencia })

        await connection.query(updateExperience, dataExperience)

        res.statusCode = 200
        res.send({ ok: 'Experiencia actualizada' })

    }
    catch (error) {
        console.error(error.message)
        res.statusCode = 500
        res.send({ error: 'No se pudo actualizar la experiencia' })
    }
}




async function viewBestExperience(req, res) {
    try {
        const connection = await getConnection()
        const [row] = await connection.query(getBestExperience)

        res.statusCode = 200
        res.send(row)

    }
    catch (error) {
        console.error(error.message);
        res.statusCode = 500
        res.send({ error: 'No se puden visualizar las experiencias, intentelo más tarde' })
    }
}

async function viewAllExperience(req, res) {
    try {
        const connection = await getConnection()
        const [row] = await connection.query(getAllExperience)

        res.statusCode = 200
        res.send(row)

    } catch (error) {
        console.error(error.message);
        res.statusCode = 500
        res.send({ error: 'No se puden visualizar las experiencias, intentelo más tarde' })
    }
}

async function viewFiveExperience(req, res) {
    try {
        const connection = await getConnection()
        const [row] = await connection.query(getFiveExperience)

        res.statusCode = 200
        res.send(row)

    }
    catch (error) {
        console.error(error.message);
        res.statusCode = 500
        res.send({ error: 'No se puden visualizar las experiencias, intentelo más tarde' })
    }
}

module.exports = { getExperiences, newExperience, editExperience, viewBestExperience, viewAllExperience, viewFiveExperience }