const joi = require ('joi')

const schemaExperience = joi.object(
    {
        nombre: joi.string().required(),
        titulo: joi.string().required(),
        descripcion: joi.string().required(),
        duracion: joi.string().required(),
        direccion: joi.string().required(),
        precio: joi.number().required(),
        plazas_totales: joi.number().required(),
        dias_actividad: joi.string().required(),
        ciudad: joi.string().required()
    }
)
const schemaUpdateExperience = joi.object(
    {
        nombre: joi.string().required(),
        descripcion: joi.string().required(),
        duracion: joi.string().required(),
        direccion: joi.string().required(),
        precio: joi.number().required(),
        plazas_totales: joi.number().required(),
        dias_actividad: joi.string().required(),
        estado: joi.boolean().required(),
        id_experiencia: joi.number().required()
    }
)

const schemaImage = joi.object(
   {
       image: joi.array().required()
   } )

async function isValidExperience(req, res, next)
{
    try
    {
        console.log(req.files);
        await schemaExperience.validateAsync(req.body)
        next()
    }
    catch (error)
    {
        console.error(error.message)
        res.statusCode = 404
        res.send({error: 'Alguno de los datos introducidos no es válido'})
    }
}
async function isValidUpdateExperience(req, res, next)
{
    try
    {
        await schemaUpdateExperience.validateAsync(req.body)
        next()
    }
    catch (error)
    {
        console.error(error.message)
        res.statusCode = 404
        res.send({error: 'Alguno de los datos introducidos no es válido'})
    }
}

async function isValidImage(req, res, next)
{
    try
    {
        console.log(req.files);
        await schemaImage.validateAsync(req.files)
        next()
    }
    catch (error)
    {
        console.error(error.message)
        res.statusCode = 404
        res.send({error: 'Alguno de los datos introducidos no es válido'})
    }
}


module.exports = { isValidExperience, isValidUpdateExperience, isValidImage }