const { getConnection } = require('../connectors.js')

const getCities= 'SELECT * FROM `ciudades`'
const cityMostExperience = 'SELECT c.id, c.nombre, count(e.nombre) as actividades FROM ciudades c INNER JOIN experiencias_ciudades ec ON c.id=ec.id_ciudad INNER JOIN experiencias e ON e.id=ec.id_experiencia GROUP BY c.id LIMIT 6'

async function showCities(req,res)
{
    try
    {
        const connection= await getConnection()
        const [row] = await connection.query(getCities)

        res.statusCode = 200
        res.send(row)
        
    }
    catch (error)
    {
        console.error(error.message)
        res.statusCode = 401
        res.send ({error: 'Ciudad no encontrada'})
    }
}

async function viewCityMoreExperiences(req,res)
{
    try
    {
        const connection = await getConnection()
        const [row] = await connection.query(cityMostExperience)

        res.statusCode = 200
        res.send(row)
    }
    catch (error)
    {
        console.error(error.message)
        res.statusCode = 500
        res.send({error: 'No se puden visualizar las ciudades con más experiencias, intentelo más tarde'})
    }
}

module.exports = {showCities, viewCityMoreExperiences }