

async function parseData(req, res, next)
{
    try
    {
        req.body = JSON.parse(req.body.data)
        next()
    }
    catch (error)
    {
        console.error(error.message)
        res.statusCode = 401
        res.send({error: 'Error inesperado, intentelo más tarde'})
    }
}

module.exports = { parseData }