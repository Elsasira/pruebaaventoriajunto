const jwt = require('jsonwebtoken')

async function login(req, res) {
    try {
        const tokenPayload = { email: req.body.email }

        const token = jwt.sign(
            tokenPayload,
            process.env.SECRET,
            { expiresIn: process.env.EXPIRE_TOKEN }
        )
        const dataUser = { ...req.dataUser, token: token }

        res.statusCode = 200
        res.send(dataUser)
    }
    catch (error) {
        res.statusCode = 500
        res.send({ error: 'Error inesperado, revisa tu conexión a internet' })
    }
}

module.exports = { login }