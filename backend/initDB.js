const { getConnection } = require('./repositories/connectors.js')

async function initDB() {

    let connection

    try {
        connection = await getConnection()

        await connection.query("CREATE DATABASE IF NOT EXISTS aventoria")
        await connection.query("USE aventoria")

        console.log(`Base de datos creada `)


        await connection.query("DROP TABLE IF EXISTS usuarios_reservas")
        await connection.query("DROP TABLE IF EXISTS reservas_experiencias")
        await connection.query("DROP TABLE IF EXISTS experiencias_ciudades")
        await connection.query("DROP TABLE IF EXISTS fotos")
        await connection.query("DROP TABLE IF EXISTS reservas")
        await connection.query("DROP TABLE IF EXISTS usuarios")
        await connection.query("DROP TABLE IF EXISTS experiencias")
        await connection.query("DROP TABLE IF EXISTS ciudades")


        await connection.query(`
            CREATE TABLE usuarios(
                id int unsigned NOT NULL AUTO_INCREMENT,
                nombre varchar(20) NOT NULL,
                apellidos varchar(100) NOT NULL,
                email varchar(100) NOT NULL,
                password varchar(60) NOT NULL,
                telefono varchar(15) NOT NULL,
                direccion varchar(300) NOT NULL,
                dni varchar(15) NOT NULL,
                fecha_nacimiento date NOT NULL,
                foto varchar(200) DEFAULT NULL,
                rol enum( 'administrador', 'registrado') DEFAULT NULL,
                PRIMARY KEY (id),
                UNIQUE KEY email (email),
                UNIQUE KEY telefono (telefono),
                UNIQUE KEY dni (dni)
            ) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
            `)

        console.log(`Tabla usuarios creada`)


        await connection.query(`
        CREATE TABLE ciudades (
            id int unsigned NOT NULL AUTO_INCREMENT,
            nombre varchar(50) NOT NULL,
            PRIMARY KEY (id)
        ) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
        `)

        console.log(`Tabla ciudades creada`)


        await connection.query(`
        CREATE TABLE  experiencias  (
            id  int unsigned NOT NULL AUTO_INCREMENT,
            nombre  varchar(50) NOT NULL,
            titulo varchar(50) NOT NULL,
            descripcion  text NOT NULL,
            duracion  varchar(20) NOT NULL,
            direccion  varchar(300) NOT NULL,
            precio  decimal(6,2) NOT NULL,
            plazas_totales  tinyint NOT NULL,
            dias_actividad  varchar(100) NOT NULL,
            estado  tinyint(1) NOT NULL,
            PRIMARY KEY ( id )
        ) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
        `)

        console.log(`Tabla experiencias creada`)


        await connection.query(`
            CREATE TABLE  reservas  (
                id  int unsigned NOT NULL AUTO_INCREMENT,
                fecha_compra  date NOT NULL,
                fecha_actividad  date NOT NULL,
                precio_total  decimal(6,2) NOT NULL,
                cantidad_reservada  tinyint NOT NULL,
                estado  enum('activa','cancelada') DEFAULT NULL,
                PRIMARY KEY ( id )
            ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
            `)

        console.log(`Tabla reservas creada`)


        await connection.query(` 
            CREATE TABLE  usuarios_reservas  (
                id_usuario  int unsigned NOT NULL,
                id_reserva  int unsigned NOT NULL,
                PRIMARY KEY ( id_usuario , id_reserva ),
                KEY  id_reserva  ( id_reserva ),
                CONSTRAINT  usuarios_reservas_ibfk_1  FOREIGN KEY ( id_usuario ) REFERENCES  usuarios  ( id ),
                CONSTRAINT  usuarios_reservas_ibfk_2  FOREIGN KEY ( id_reserva ) REFERENCES  reservas  ( id )
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci 
            `)

        console.log(`Tabla usuarios_reservas creada`)


        await connection.query(`
            CREATE TABLE  reservas_experiencias  (
                id_reserva  int unsigned NOT NULL,
                id_experiencia  int unsigned NOT NULL,
                comentario  text,
                valoracion  enum( '1', '2', '3', '4', '5') DEFAULT NULL,
                estado  enum( 'pendiente','aprobado') DEFAULT NULL,
                PRIMARY KEY ( id_reserva , id_experiencia ),
                KEY  id_experiencia  ( id_experiencia ),
                CONSTRAINT  reservas_experiencias_ibfk_1  FOREIGN KEY ( id_reserva ) REFERENCES  reservas  ( id ),
                CONSTRAINT  reservas_experiencias_ibfk_2  FOREIGN KEY ( id_experiencia ) REFERENCES  experiencias  ( id )
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
            `)

        console.log(`Tabla reservas_experiencias creada`)


        await connection.query(`
            CREATE TABLE  fotos  (
                id  int unsigned NOT NULL AUTO_INCREMENT,
                id_experiencia  int unsigned DEFAULT NULL,
                descripcion  text,
                ruta  varchar(200) NOT NULL,
                PRIMARY KEY ( id ),
                KEY  id_experiencia  ( id_experiencia ),
                CONSTRAINT  fotos_ibfk_1  FOREIGN KEY ( id_experiencia ) REFERENCES  experiencias  ( id )
            ) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci 
        `)

        console.log(`Tabla fotos creada`)



        await connection.query(`
            CREATE TABLE  experiencias_ciudades  (
                id_experiencia  int unsigned NOT NULL,
                id_ciudad  int unsigned NOT NULL,
                PRIMARY KEY ( id_experiencia , id_ciudad ),
                KEY  id_ciudad  ( id_ciudad ),
                CONSTRAINT  experiencias_ciudades_ibfk_1  FOREIGN KEY ( id_experiencia ) REFERENCES  experiencias  ( id ),
                CONSTRAINT  experiencias_ciudades_ibfk_2  FOREIGN KEY ( id_ciudad ) REFERENCES  ciudades  ( id )
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci 
        `)

        console.log(`Tabla experiencias_ciudades creada`)


    } catch (e) {
        console.error(e.message)
    } finally {
        if (connection) {
            connection.release();
        }
        console.log('done');
        process.exit()
    }


}

initDB()
